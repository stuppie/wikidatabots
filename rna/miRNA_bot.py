import sys
import pandas as pd
import pprint
import time
import numpy as np

import PBB_Core
import PBB_login

"""
A bot for import of human microRNAs. It creates new Wikidata items and links the miRNAs to the genes they are being
encoded by. Furthermore, it establishes links to
"""

__author__ = 'Sebastian Burgstaller-Muehlbacher'
__license__ = 'AGPLv3'
__copyright__ = 'Sebastian Burgstaller-Muehlbacher'

entrez_qid_map = {}

# mirtar_base = pd.read_csv('~/Downloads/hsa_MTI.csv', header=0, sep=',')
mirtar_base = pd.read_csv('./data/hsa_MTI.csv', low_memory=False,
                          dtype={'References (PMID)': np.str, 'Target Gene (Entrez Gene ID)': np.str})

mirbase_data = pd.read_csv('./mirbase_data/hsa.gff3', sep='\t', low_memory=False, skiprows=13, index_col=None,
                           names=['chr', 'ph', 'type', 'start', 'stop', 'ph2', 'orientation', 'ph3', 'info'],
                           dtype={'start': np.str, 'stop': np.str})

enc_map = pd.read_csv('./mirbase_data/mirna_context.txt', sep='\t', low_memory=False, skiprows=13, index_col=None,
                           names=['id', 'transcript', 'orientation', 'part', 'chr', 'orientation', 'hgnc', 'clone'])

print(enc_map.head())

chromosome_map = {
    'chr1': 'Q430258',
    'chr2': 'Q638893',
    'chr3': 'Q668633',
    'chr4': 'Q836605',
    'chr5': 'Q840741',
    'chr6': 'Q540857',
    'chr7': 'Q657319',
    'chr8': 'Q572848',
    'chr9': 'Q840604',
    'chr10': 'Q840737',
    'chr11': 'Q847096',
    'chr12': 'Q847102',
    'chr13': 'Q840734',
    'chr14': 'Q138955',
    'chr15': 'Q765245',
    'chr16': 'Q742870',
    'chr17': 'Q220677',
    'chr18': 'Q780468',
    'chr19': 'Q510786',
    'chr20': 'Q666752',
    'chr21': 'Q753218',
    'chr22': 'Q753805',
    'chrX': 'Q61333',
    'chrY': 'Q202771',
    'chrMT': 'Q27075'
}


def get_entrez_qid_map(prop_nr):
    query = '''
        SELECT * WHERE {{
            ?qid wdt:{} ?id .
            ?qid wdt:P703 wd:Q15978631 .

            OPTIONAL {{
                ?qid wdt:P353 ?id .
            }}
        }}
        '''.format(prop_nr)

    results = PBB_Core.WDItemEngine.execute_sparql_query(query=query)['results']['bindings']

    id_wd_map = dict()
    hgnc_map = dict()
    for z in results:
        id_wd_map[z['id']['value']] = z['qid']['value'].split('/')[-1]
        if 'hgnc' in z:
            hgnc_map[z['hgnc']['value']] = z['qid']['value'].split('/')[-1]

    return id_wd_map, hgnc_map


def make_refs(mirna_label, mirna_id, prop_nr, database):
    refs = [[
        PBB_Core.WDItemID(value=database, prop_nr='P248', is_reference=True),  # stated in
        PBB_Core.WDExternalID(value=mirna_id, prop_nr=prop_nr, is_reference=True),  # source element
        PBB_Core.WDItemID(value='Q1860', prop_nr='P407', is_reference=True),  # language of work
        PBB_Core.WDMonolingualText(value=mirna_label, language='en', prop_nr='P1476', is_reference=True),
        PBB_Core.WDTime(time=time.strftime('+%Y-%m-%dT00:00:00Z'), prop_nr='P813', is_reference=True)  # retrieved
    ]]

    return refs


def get_gene(chr, start, stop):
    query = '''
            SELECT DISTINCT ?gene ?geneLabel ?start ?stop WHERE {{
              ?gene wdt:P1057 wd:{} .
              ?gene wdt:P279 wd:Q7187 .

              ?gene p:P644 ?statement .
              ?gene wdt:P703 wd:Q15978631 .
              ?gene wdt:P2548 ?strand_orientation .

              ?statement v:P644 ?start .
              ?statement q:P659 wd:Q20966585 .

              ?gene p:P645 ?statement2 .
              ?statement2 v:P645 ?stop .
              ?statement2 q:P659 wd:Q20966585 .

              SERVICE wikibase:label {{
                    bd:serviceParam wikibase:language "en" .
              }}

              FILTER (xsd:integer(?start) >= {} && xsd:integer(?stop) <= {})
            }}
            ORDER BY ?geneLabel
            '''.format(chr, start, stop)

    results = PBB_Core.WDItemEngine.execute_sparql_query(query=query)['results']['bindings']
    mir_gene = any([x['geneLabel']['value'].startswith('MIR') for x in results])

    gene_qids = []
    for x in results:
        if x['geneLabel']['value'].startswith('MIR'):
            return [x['gene']['value'].split('/')[-1]]
        else:
            gene_qids.append(x['gene']['value'].split('/')[-1])

    return gene_qids


def generate_targets():
    data = []
    let_7b_5p = mirtar_base.loc[mirtar_base['miRNA'].values == 'hsa-let-7b-5p', :]
    let_7b_5p = let_7b_5p[['miRTarBase ID', 'miRNA', 'Target Gene (Entrez Gene ID)']].drop_duplicates()
    print(let_7b_5p.count())
    print(len(let_7b_5p['miRTarBase ID'].unique()))
    for count, mir in let_7b_5p.iterrows():

        acc = mir['miRTarBase ID']
        ncbi_id = mir['Target Gene (Entrez Gene ID)']
        mirna_label = mir['miRNA']

        if ncbi_id not in entrez_qid_map:
            continue

        print(acc, ncbi_id)

        refs = [[
            PBB_Core.WDItemID(value='Q6826951', prop_nr='P248', is_reference=True),  # stated in
            PBB_Core.WDExternalID(value=acc, prop_nr='P2646', is_reference=True),  # source element
            PBB_Core.WDItemID(value='Q1860', prop_nr='P407', is_reference=True),  # language of work
            PBB_Core.WDMonolingualText(value=mirna_label, language='en', prop_nr='P1476', is_reference=True),
            PBB_Core.WDTime(time=time.strftime('+%Y-%m-%dT00:00:00Z'), prop_nr='P813', is_reference=True)  # retrieved
        ]]

        stmnt = PBB_Core.WDItemID(value=entrez_qid_map[ncbi_id], prop_nr='P128', references=refs)
        data.append(stmnt)


def main():
    entrez_qid_map, hgnc_qid_map = get_entrez_qid_map('P351')

    # login_obj = PBB_login.WDLogin(user='ProteinBoxBot', pwd=sys.argv[1])
    login_obj = PBB_login.WDLogin(user='sebotic', pwd='4r,A^8lBq$n]')

    premir_map = {}
    matmir_map = {}

    for count, premir in mirbase_data.iterrows():
        start_time = time.time()
        chromosome = premir['chr']
        start = premir['start']
        stop = premir['stop']
        orientation = premir['orientation']

        if count == 15:
            break

        if premir['type'] == 'miRNA_primary_transcript':
            print(premir['info'])
            gene_qids = set(get_gene(chr=chromosome_map[chromosome], start=start, stop=stop))
            print(gene_qids)

            mirna_id, alias, name = [x.split('=')[1] for x in premir['info'].split(';')]

            genome_build_qualifier = [PBB_Core.WDItemID(value='Q20966585', prop_nr='P659', is_qualifier=True)]
            strand_orientation = 'Q22809680' if orientation == '+' else 'Q22809711'

            refs = make_refs(mirna_label=name, mirna_id=mirna_id, prop_nr='P2870', database='Q6826947')
            data = [
                PBB_Core.WDItemID(value='Q310899', prop_nr='P279', references=refs),
                PBB_Core.WDItemID(value='Q15978631', prop_nr='P703', references=refs),
                PBB_Core.WDExternalID(value=mirna_id, prop_nr='P2870', references=refs),
                PBB_Core.WDItemID(value=chromosome_map[chromosome], prop_nr='P1057', references=refs),  # chromosome
                PBB_Core.WDString(value=start, prop_nr='P644', references=refs, qualifiers=genome_build_qualifier),
                PBB_Core.WDString(value=stop, prop_nr='P645', references=refs, qualifiers=genome_build_qualifier),
                PBB_Core.WDItemID(value=strand_orientation, prop_nr='P2548', references=refs,
                                  qualifiers=genome_build_qualifier),
            ]

            for gene in gene_qids:
                data.append(PBB_Core.WDItemID(value=gene, prop_nr='P702', references=refs))

            try:
                wd_item = PBB_Core.WDItemEngine(item_name='miRNA', domain='microRNAs', data=data)

                wd_item.set_label(name)
                wd_item.set_description('human precursor microRNA')
                if alias != name:
                    wd_item.set_aliases([alias], append=True)

                qid = wd_item.write(login_obj)
                premir_map.update({mirna_id: qid})

                print('Successfull write to item ', qid, 'count:', count, '##' * 20)
                PBB_Core.WDItemEngine.log(
                    'INFO', '{main_data_id}, "{exception_type}", "{message}", {wd_id}, {duration}'
                        .format(
                            main_data_id='{}'.format(mirna_id),
                            exception_type='',
                            message='success{}'.format(''),
                            wd_id=qid,
                            duration=time.time() - start_time
                        ))

                for x in gene_qids:
                    enc = PBB_Core.WDItemID(value=qid, prop_nr='P688', references=refs)
                    gene_item = PBB_Core.WDItemEngine(wd_item_id=x, data=[enc], append_value=['P688'])
                    gene_item.write(login_obj)
                    print('Encodes written for gene item:', enc)

            except Exception as e:
                print(e)

                PBB_Core.WDItemEngine.log(
                    'ERROR', '{main_data_id}, "{exception_type}", "{message}", {wd_id}, {duration}'
                        .format(
                            main_data_id='{}'.format(mirna_id),
                            exception_type=type(e),
                            message=e.__str__(),
                            wd_id='',
                            duration=time.time() - start_time
                        ))

    for count, mir in mirbase_data.iterrows():
        start_time = time.time()
        chromosome = mir['chr']
        start = mir['start']
        stop = mir['stop']
        orientation = mir['orientation']

        if count == 15:
            break

        if mir['type'] == 'miRNA':
            print(mir['info'])
            # gene_qids = set(get_gene(chr=chromosome_map[chromosome], start=start, stop=stop))
            # print(gene_qids)

            mirna_id, alias, name, parent = [x.split('=')[1] for x in mir['info'].split(';')]
            mirna_id = mirna_id.split('_')[0]

            genome_build_qualifier = [
                PBB_Core.WDItemID(value='Q20966585', prop_nr='P659', is_qualifier=True)]
            strand_orientation = 'Q22809680' if orientation == '+' else 'Q22809711'

            refs = make_refs(mirna_label=name, mirna_id=mirna_id, prop_nr='P2871', database='Q6826947')
            data = [
                PBB_Core.WDItemID(value='Q23838648', prop_nr='P279', references=refs),
                PBB_Core.WDItemID(value='Q15978631', prop_nr='P703', references=refs),
                PBB_Core.WDExternalID(value=mirna_id, prop_nr='P2871', references=refs),
                PBB_Core.WDItemID(value=chromosome_map[chromosome], prop_nr='P1057', references=refs),
                PBB_Core.WDString(value=start, prop_nr='P644', references=refs,
                                  qualifiers=genome_build_qualifier),
                PBB_Core.WDString(value=stop, prop_nr='P645', references=refs,
                                  qualifiers=genome_build_qualifier),
                PBB_Core.WDItemID(value=strand_orientation, prop_nr='P2548', references=refs,
                                  qualifiers=genome_build_qualifier),
                PBB_Core.WDItemID(value=premir_map[parent], prop_nr='P361', references=refs)
            ]

            ap = ['P361', chr, coords]
            try:
                if mirna_id in matmir_map:
                    wd_item = PBB_Core.WDItemEngine(wd_item_id=matmir_map[mirna_id], domain='microRNAs', data=data,
                                                    append_value=['P361'])
                else:
                    wd_item = PBB_Core.WDItemEngine(item_name='miRNA', domain='microRNAs', data=data,
                                                    append_value=['P361'])

                wd_item.set_label(name)
                wd_item.set_description('human mature microRNA')
                if alias != name:
                    wd_item.set_aliases([alias], append=True)

                qid = wd_item.write(login_obj)
                matmir_map.update({mirna_id: qid})

                print('Successfull write to item ', qid, 'count:', count, '##' * 20)
                PBB_Core.WDItemEngine.log(
                    'INFO', '{main_data_id}, "{exception_type}", "{message}", {wd_id}, {duration}'
                        .format(
                            main_data_id='{}'.format(mirna_id),
                            exception_type='',
                            message='success{}'.format(''),
                            wd_id=qid,
                            duration=time.time() - start_time
                    ))

                if parent in premir_map:
                    p = PBB_Core.WDItemID(value=qid, prop_nr='P527', references=refs)
                    gene_item = PBB_Core.WDItemEngine(wd_item_id=premir_map[parent], data=[p], append_value=['P527'])
                    gene_item.write(login_obj)

            except Exception as e:
                print(e)

                PBB_Core.WDItemEngine.log(
                    'ERROR', '{main_data_id}, "{exception_type}", "{message}", {wd_id}, {duration}'
                        .format(
                            main_data_id='{}'.format(mirna_id),
                            exception_type=type(e),
                            message=e.__str__(),
                            wd_id='',
                            duration=time.time() - start_time
                    ))

    # wd_item = PBB_Core.WDItemEngine(wd_item_id='Q21414101', data=data)
    # wd_item.write(login_obj)

    # pprint.pprint(mirtar_base.count())

    # unique_mirs = mirtar_base['miRNA'].unique()
    #
    # for mir in unique_mirs:
    #
    #     # references = generate_refs(chembl_id)
    #
    #     curr_mir_df = unique_mirs[unique_mirs['miRNA'] == mir]
    #
    #     statements = list()
    #
    #     # mature miRNA Q23838648
    #
    #     for x in curr_mir_df.index:
    #         curr_mesh = curr_mir_df.loc[x, 'mesh_id']
    #         if pd.notnull(curr_mesh) and curr_mesh in mesh_wd_map:
    #             print(chembl_id, curr_mesh, 'found')



if __name__ == '__main__':
    sys.exit(main())
