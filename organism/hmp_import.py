import datetime

from ProteinBoxBot_Core import PBB_Core, PBB_login
from ProteinBoxBot_Core.PBB_Core import WDApiError
from SPARQLWrapper import SPARQLWrapper, JSON
import json
SERVER = "www.wikidata.org"
z = {'airways': 'Q13400765',  # respiratory tract
     'blood': 'Q7873',
     'bone': 'Q265868',
     'ear': 'Q7362',
     'eye': 'Q7364',
     'gastrointestinal_tract': 'Q6431240',  # gut
     'heart': 'Q1072',
     'liver': 'Q9368',
     'lymph_nodes': 'Q170758',  # lymph node
     'nose': 'Q7363',
     'oral': 'Q9635',  # mouth
     'other': None,
     'skin': 'Q1074',
     'unknown': None,
     'urogenital_tract': 'Q4712977',  # genitourinary system
     'wound': 'Q184753'
     }

import pandas as pd

pd.set_option('display.width', 180)
df = pd.read_csv("organism/hmpdacc.csv")

# Start with NCBI status == 6 or 4
df['NCBI Submission Status'].value_counts()
df['NCBI Submission Status'] = df['NCBI Submission Status'].apply(lambda x: x[0]).astype(int)
df=df[(df['NCBI Submission Status'] == 6) | (df['NCBI Submission Status'] == 4)]

# Drop any that have no genes
df = df.dropna(subset=["Genbank ID"])

#  These don't have taxids.. of course. Why would they?
# But we can get it from the NCBI taxonomy dmp
# Run taxonomy_parser.py

with open("organism/flat_files/tax.json") as f:
    tax = json.load(f)
tax = {int(k):v for k,v in tax.items()}
tax_name = {v['scientific_name']:v for k,v in tax.items()}
df['taxid'] = df['Organism Name'].apply(lambda x: tax_name.get(x.lower(),{}).get('taxid',0)).astype(int)

# how many are already in wd?
query = """
    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
    SELECT ?item ?taxid
    WHERE{ ?item wdt:P685 ?taxid }
"""
endpoint = SPARQLWrapper("https://query.wikidata.org/bigdata/namespace/wdq/sparql")
endpoint.setQuery(query)
endpoint.setReturnFormat(JSON)
response = endpoint.query().convert()['results']['bindings']
wdtx = {int(x['taxid']['value']) for x in response}

# ones not in wd
print("{}/{}".format(len(set(df['taxid']) - wdtx), len(df)))

# theres still a lot that have tax IDs but the name is different....
#  Acinetobacter baumannii ATCC 19606 | Acinetobacter baumannii ATCC 19606 = CIP 70.34 = JCM 6841
from Bio import Entrez
from bs4 import BeautifulSoup
import lxml
Entrez.email = "stuppie@gmail.com"
r=Entrez.efetch('bioproject', id="38509,33011", retmode="xml")
soup = BeautifulSoup(r.read(), 'xml')

for record in soup.find_all("Project"):
    print(record.find("ArchiveID").attrs['id'])
    organism = record.find("Organism")
    organism.find("OrganismName").contents[0]
    print(organism.attrs['taxID'])


# Columns that we want to add:
# ["taxid", "Organism Name", "HMP Isolation Body Site"]
# plus columns from the NCBI taxonomy dump
# ["rank", "parent_taxid"]
df = df[["taxid", "Organism Name", "HMP Isolation Body Site"]]
df['rank'] = df['taxid'].apply(lambda x:tax[x]['rank'])



def create_reference(taxid):
    ref_stated_in = PBB_Core.WDItemID("Q13711410", 'P248', is_reference=True)
    ref_retrieved = PBB_Core.WDTime(datetime.date.today().strftime("+%Y-%m-%dT00:00:00Z"), 'P813', is_reference=True)
    ref_taxid = PBB_Core.WDString(taxid, "P685", is_reference=True)
    reference = [ref_stated_in, ref_retrieved, ref_taxid]
    for ref in reference:
        ref.overwrite_references = True
    return reference

def create_item():
    statements = [PBB_Core.WDExternalID(value=self.id, prop_nr="P685", references=[self.reference]),
                  PBB_Core.WDItemID(value=IPRItem.type2subclass[self.type], prop_nr="P279",
                                    references=[self.reference])]